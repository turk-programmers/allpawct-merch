<?php
// 
// Set a require state, every files include or reuire in ths file will know they are in require state and will not execute some skipped parts.
// For example index.php file will be required in this file to get all constant variables, and skip all processing parts.
// 
$in_a_require_state = true;
require_once('../index.php');

require_once($application_folder.'/../assets/core-less-css.php');
