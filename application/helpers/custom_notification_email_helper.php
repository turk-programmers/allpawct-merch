<?php
function prepare_email_content(){
    $CI = & get_instance();
    $user_session = $CI->session->get_user();

    // 
    // Purchase Information
    // 
    $CI->tablemessage->putMesTbl('[clear]');
    foreach ($CI->cart->contents() as $pid => $product) {
        /* insert image */
        $img = "<img class='img-responsive ' height='70' src='https://fnetclients.s3.amazonaws.com/merchant/clients/".$CI->shop['client_id']."/pictures/".$product['options']['thumb']."' alt='".$product['name']."'>";
            $CI->tablemessage->putMesTbl($img);
            $choice = "";
            if(@$product['options']['advance_price']['use-advance-pricing']) {
                $choice = ' (' . @$product['options']['advance_price']['prices'][$product['options']['selected_price']]['label'] . ')';
            } elseif(@$product['options']['price2'] > 0 or @$product['options']['price3'] > 0) {
                $choice = " (".$settings['shop_config']['priceLabel'][$product['options']['selected_price']].")";
            }
            $CI->tablemessage->putMesTbl($product['qty'] . 'x ' . $product['name'] . $choice, '$' . number_format($product['price'] * $product['qty'], 2));
        if(count($product['selected_options'])) {
            foreach((array)$product['selected_options'] as $option) {
                $CI->tablemessage->putMesTbl('[indent]', @$option['label'].": ".@$option['value']);
            }
        }
    }
    // $CI->tablemessage->putMesTbl();
    $CI->tablemessage->putMesTbl('<b>Sub Total</b>', "$".number_format($user_session['summary']['totalWithTax'], 2));
    $CI->tablemessage->putMesTbl('<b>Sales Tax ('.@$user_session['salestax_rate']['tax_rate'].'%)</b>', "$".number_format($user_session['summary']['taxAmount'], 2));
    $CI->tablemessage->putMesTbl('<b>Delivery Fee</b>', "$".number_format($user_session['summary']['deliveryFee'], 2));
    $purchaseinfo = $CI->tablemessage->putMesTbl('<b>Total</b>', "$".number_format($user_session['summary']['total'], 2));
    $CI->session->set_user('purchaseinfo', $purchaseinfo);

    // 
    // Contact
    // 
    $CI->tablemessage->putMesTbl('[clear]');
    $CI->tablemessage->putMesTbl("<b>Name</b>", $user_session['billing']['firstname'] ." ". $user_session['billing']['lastname']);
    $CI->tablemessage->putMesTbl("<b>Email</b>", $user_session['billing']['email']);
    $CI->tablemessage->putMesTbl("<b>Address</b>", $user_session['billing']['streetaddress'].' '.$user_session['billing']['city'].', '.$user_session['billing']['state'] ." ". $user_session['billing']['zip'] );
    $contact = $CI->tablemessage->putMesTbl("<b>Daytime Phone</b>", $user_session['billing']['daytimephone']);
    $CI->session->set_user('contact', $contact);

    // 
    // Purchased By
    // 
    $CI->session->set_user('purchasedby', $contact);

    // 
    // Deliver to
    // 
    $CI->tablemessage->putMesTbl('[clear]');
    $CI->tablemessage->putMesTbl('<b>Location</b>', $user_session['delivery']['deliver_firstname'].' '.$user_session['delivery']['deliver_lastname']);
    $deliveryto = $CI->tablemessage->putMesTbl('<b>Address</b>', $user_session['delivery']['deliver_streetaddress'].' '.$user_session['delivery']['deliver_city'].', '.$user_session['delivery']['deliver_state'].' '.$user_session['delivery']['deliver_zip']);
    $CI->session->set_user('deliveryto', $deliveryto);

    // 
    // Delivery
    // 
    $CI->tablemessage->putMesTbl('[clear]');
    $CI->tablemessage->putMesTbl('<b>Location</b>', $user_session['delivery']['deliver_firstname'].' '.$user_session['delivery']['deliver_lastname']);
    $deliverymsg = $CI->tablemessage->putMesTbl('<b>Address</b>', $user_session['delivery']['deliver_streetaddress'].' '.$user_session['delivery']['deliver_city'].', '.$user_session['delivery']['deliver_state'].' '.$user_session['delivery']['deliver_zip']);
    $CI->session->set_user('deliverymsg', $deliverymsg);

    // 
    // Description
    // 
    $CI->tablemessage->putMesTbl('[clear]');
    foreach ($CI->cart->contents() as $pid => $product) {
        $CI->tablemessage->putMesTbl($product['qty'] . 'x ' . $product['name'], '$' . number_format($product['price'] * $product['qty'], 2));
    }
    $CI->tablemessage->putMesTbl('<b>Ordered By</b>',$user_session['billing']['firstname'] ." ". $user_session['billing']['lastname']);
    $CI->tablemessage->putMesTbl("<b>Message To Appear On Card</b>",$user_session['delivery']['deliver_message']);
    $description = $CI->tablemessage->putMesTbl("<b>Special Instructions</b>",$user_session['delivery']['deliver_instructions']);
    $CI->session->set_user('description', $description);


    $CI->session->set_user('shopname', $CI->shop['label']);
    $CI->session->set_user('date', date('F d, Y'));
    $CI->session->set_user('url', base_url());
    $user_session = $CI->session->get_user();
}