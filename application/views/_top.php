
<?php

function get_wrapper_cache($wrapper_link)
{
    $wrapper = '';

    // Get wrapper
    $cache_filename = 'application/cache/template.html';
    $cache_time = 3 * 3600; // Time in seconds to keep a page cached
    // Check Cache
    $cache_created = (file_exists($cache_filename)) ? filemtime($cache_filename) : 0;
    // If cache valid, get it
    if ((time() - $cache_created) < $cache_time) {
        //readfile($cache_filename);
        $wrapper = @file_get_contents($cache_filename);
    }
    // If no cache, get wrapper and cache it
    if (empty($wrapper) or is_dev()) {
        $wrapper = @file_get_contents($wrapper_link);

        file_put_contents($cache_filename, $wrapper);
    }
    return $wrapper;
}
/*
$wrapper = get_wrapper_cache(TEMPLATE_URL);
$wrapper = str_replace('<head>', '<head><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" 
integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" 
integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">', $wrapper);

$wrapper = str_replace('src="/', 'src="' . EXTERNAL_SCRIPT_URL, $wrapper);
$wrapper = str_replace('src="../', 'src="' . EXTERNAL_SCRIPT_URL, $wrapper);
$wrapper = str_replace('<a href="/', '<a href="' . HOME_PAGE, $wrapper);
$wrapper = str_replace('href="/', 'href="' . EXTERNAL_SCRIPT_URL, $wrapper);
$wrapper = str_replace('doRedirect();', '', $wrapper);
$wrapper = str_replace('http://maps.googleapis.com', 'https://maps.googleapis.com', $wrapper);
$wrapper = str_replace("var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;", "", $wrapper);
$wrapper = str_replace("po.src = 'https://cdnjs.cloudflare.com/ajax/libs/autotrack/2.4.1/autotrack.js';", "", $wrapper);
$wrapper = str_replace("var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);", "", $wrapper);
$wrapper = str_replace(HOME_PAGE . 'merchandise/merchandise', 'https://allpawct.funeralnetcustomsolutions.com/merchandise/products/Urns/#urns-view', $wrapper);
$wrapper = str_replace("Don't delete this page" , '', $wrapper);
$wrapper = str_replace('merchandise_funeralnet', '' , $wrapper);

$content_pos = strpos($wrapper, '<!--merchant-->');
$wrapper_top = substr($wrapper, 0, $content_pos);
define('WRAPPER_BOTTOM', substr($wrapper, $content_pos + 15));

echo $wrapper_top;
*/
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5.0">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" 
        integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" 
        integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    </head>
    <body>

        <header class="site-header">
            <div class="container-fluid">
                <div class="site-header__top">
                    <a class="site-header__top-back" href="<?php echo HOME_PAGE;?>">
                          <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="home" class="svg-inline--fa fa-home fa-w-18" role="img" viewBox="0 0 576 512" style="width: 20px; height: auto;"><path fill="currentColor" d="M280.37 148.26L96 300.11V464a16 16 0 0 0 16 16l112.06-.29a16 16 0 0 0 15.92-16V368a16 16 0 0 1 16-16h64a16 16 0 0 1 16 16v95.64a16 16 0 0 0 16 16.05L464 480a16 16 0 0 0 16-16V300L295.67 148.26a12.19 12.19 0 0 0-15.3 0zM571.6 251.47L488 182.56V44.05a12 12 0 0 0-12-12h-56a12 12 0 0 0-12 12v72.61L318.47 43a48 48 0 0 0-61 0L4.34 251.47a12 12 0 0 0-1.6 16.9l25.5 31A12 12 0 0 0 45.15 301l235.22-193.74a12.19 12.19 0 0 1 15.3 0L530.9 301a12 12 0 0 0 16.9-1.6l25.5-31a12 12 0 0 0-1.7-16.93z"/></svg> Back to home page
                    </a>
                </div>
                <div class="site-header__bottom">
                    <a href="<?php echo HOME_PAGE;?>">
                        <img class="logo" src="/merchandise/assets/images/logo.png" />
                    </a>
                </div>
            </div>
        </header>
        <main>
            <div class="site-header__banner">
                <div class="container">
                    <h1 class="site-header__banner-title">Merchandise</h1>
                </div>
            </div>
             <div class="container-fluid">
                <div class="container">