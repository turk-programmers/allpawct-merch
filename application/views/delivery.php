
<script>
    $(function() {
<?php
if (@$prev_form) {
    ?>
            $.mc.prev_form = '<?= $prev_form ?>';
    <?php
}
?>
        $.mc.next_form = '<?= $next_form ?>';
    });

    var dateFormat = /^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.]((?:19|20)\d\d)$/
    //var dateFormat = /^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.]((?:19|20)\d\d)$/;

    function checkLEGAL(field, rules, i, options){
            var match = field.val().match(dateFormat)

            if (!match) return "Invalid date";
            var bd=new Date(match[3], match[2]-1, match[1]);

            var diff = Math.floor((new Date).getTime() - bd.getTime());
            var day = 1000* 60 * 60 * 24;

            var days = Math.floor(diff/day);
            var months = Math.floor(days/31);
            var years = Math.floor(months/12);
    }
</script>

<div id="merchandise_wrapper">
    <?php $this->view('static-views/breadcrumb', array('shops' => $shops)); ?>
    <!-- <div class=''>
        <div class='col-lg-12'>
            <p>Substitutions of an arrangement and container of similar value may be made in the unlikely event that your selection is unavailable. </p>

        </div>
    </div> -->

    <div class='container-delivery'>
        <div class='row'>
            <form role="form" action="" method="post" id='mainform'>
                <input type="hidden" name="scriptaction"    id="scriptaction"   value="validate" />
                <input type="hidden" name="next_form"       id="next_form"      value="<?= $next_form ?>" />


                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-11">

                        <legend>Delivery Address</legend>

                        <?php if ($shops['show_locations'] == '1') { ?>
                            <div class='form-group'>
                                <label for='state'>Deliver To</label>
                                <select class='form-control deliverto validate[required]' name='deliverto'>
                                    <option value='' data-street='' data-city='' data-state='' data-zip='' data-salestax='' data-default-salestax='' data-name='' >Please select the location</option>
                                    <?php foreach ($locations as $key => $group): ?>
                                        <!--
                                        <?php print_r($group) ?>
                                        -->
                                        <option value='<?php echo $group['id'] ?>' data-street='<?php echo $group['address_street'] ?>' data-city='<?php echo $group['address_city'] ?>' data-state='<?php echo $group['address_state'] ?>' data-zip='<?php echo $group['address_zip'] ?>' data-salestax='<?php echo $group['tax_rate'] ?>' data-default-salestax='' data-name='<?php echo $group['name_id'] ?>' ><?php echo $group['name_id'] ?></option>
                                    <?php endforeach ?>
                                    <option value='other' data-street='' data-city='' data-state='' data-zip='' data-salestax='' data-default-salestax='' data-name='' >Other</option>
                                </select>
                            </div>
                        <?php } ?>

                        <div class='form-group hidden'>
                            <button class='btn btn-default btn-sm btn-block billaddress'>Deliver to my billing address</button>
                        </div>

                        <div class='form-group'>
                            <!-- <label for='datedelivery'>Date of Service/Delivery</label> -->
                            <!-- <input type="text" type="text" name='datedelivery' id='datedelivery' class="form-control datedelivery validate[required,funcCall[checkLEGAL]]"> -->
                            <input type="hidden" name='datedelivery' id='datedelivery' class="" value="<?=date('m-d-Y', strtotime('+3day'))?>">
                            
                            <!-- <p class='bg-warning messageShow'></p> -->

                        </div>

                        <div class='form-group'>
                            <label for='deliver_firstname'>First Name</label>
                            <input type='text' name='deliver_firstname' id='deliver_firstname' class='tmpdata form-control validate[required]' placeholder='First Name'/>
                        </div>

                        <div class='form-group'>
                            <label for='deliver_lastname'>Last Name</label>
                            <input type='text' name='deliver_lastname' id='deliver_lastname' class='tmpdata form-control validate[required]' placeholder='Last Name'/>
                        </div>

                        <div class='form-group'>
                            <label for='deliver_email'>E-mail</label>
                            <input type='text' class='tmpdata form-control validate[required,custom[email]]' name='deliver_email' id='deliver_email' placeholder='E-mail' data-value='kapom@frazerconsultants.com' />
                        </div>

                        <div class='form-group'>
                            <label for='deliver_streetaddress'>Street Address</label>
                            <input type='text' class='tmpdata form-control validate[required]' name='deliver_streetaddress' id='deliver_streetaddress' placeholder='Street Address'/>
                        </div>


                        <div class='form-group'>
                            <label for='deliver_city'>City</label>
                            <input type='text' class='tmpdata form-control validate[required]' name='deliver_city' id='deliver_city' placeholder='City'/>
                        </div>

                        <div class='form-group'>
                            <label for='deliver_state'>State</label>
                            <select class='tmpdata form-control validate[required]' name='deliver_state' id='deliver_state'>
                                <?php foreach ($cfg['states'] as $key => $group): ?>
                                    <option value='<?php echo $group['code'] ?>'><?php echo $group['name'] ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>

                        <div class='form-group'>
                            <label for='deliver_zip'>Zip</label>
                            <input type='text' class='tmpdata form-control validate[required,custom[onlyLetterNumber]]' name='deliver_zip' id='deliver_zip' placeholder='Zip'/>
                        </div>

                        <div class='form-group'>
                            <label for='deliver_daytimephone'>Daytime Phone</label>
                            <input type='text' class='tmpdata form-control validate[required]' name='deliver_daytimephone' id='deliver_daytimephone' placeholder='Daytime Phone'/>
                        </div>

                        <!-- <div class='form-group'>
                            <label for='deliver_decedent'>Decedent's Name</label>
                            <input type='text' class='form-control validate' name='deliver_decedent' id='deliver_decedent' placeholder='Decedent'/>
                        </div> -->

                        <div class='form-group'>
                            <label for='deliver_message'>Message To Appear On Card</label>
                            <textarea id='deliver_message' class='form-control' name='deliver_message' placeholder='Enter Message To Appear On Card'></textarea>
                        </div>

                        <div class='form-group'>
                            <label for='deliver_instructions'>Special Instructions</label>
                            <textarea id='deliver_instructions' class='form-control' name='deliver_instructions' placeholder='Enter Special Instructions'></textarea>
                        </div>

                    </div>

                    <?php $this->load->view('summary_cart'); ?> 
                
            </form>
        </div>
    </div>
    <div class='row col-lg-6 pull-right'>
        <a class='merchant-btn btn-back' href="#">Return to Billing</a>
        <a class='merchant-btn-revert btn-next' style='display: none' href="#" >Continue</a>
    </div>
</div>
<?php
$message = $shops['same_day_message'];

?>
<script>var tmpdata = {};</script>
<script src="<?=ASSETPATH?>/js/delivery.js<?=TAILSTRING?>"></script>
<script>
    $(function() {
        $('.billaddress').click(function() {
            //return false;
            //$('#mainform').clearForm({callback:function(){ $('.messageShow').html('').parent().removeClass('has-warning'); }});
            $("#mainform").jsonToElement({
                json: <?= json_encode($user_session['billing']['autopopulate']) ?>,
                show: true
            });
            return false;
        });

    <?php
    if (!empty($user_session['delivery'])) { 
        $deliverydate = date('m-d-Y',strtotime(str_replace("-","/",$user_session['delivery']['datedelivery'])));
        $user_session['delivery']['datedelivery'] = $deliverydate;
        ?>
        tmpdata = <?= json_encode($user_session['delivery']) ?>;
        <?php
        if($user_session['delivery']['deliverto'] != 'other') { 
            ?>
            $("#mainform").jsonToElement({
                json: <?= json_encode($user_session['delivery']) ?>,
            });
            $('.tmpdata').each(function(){
                $(this).parent().hide();
            });
            <?php 
        } else { 
            ?>
            $("#mainform").jsonToElement({
                json: <?= json_encode($user_session['delivery']) ?>,
                show: true
            });

            <?php 
        } 
        ?>
        $('.btn-next').show();
        <?php 
    }
    ?>

    // $('#datedelivery').datepicker({
    //     format: 'mm-dd-yyyy',
    //     startDate: "<?= $now ?>",
    // }).on('changeDate', function(ev) {
    //     $('.datepicker').hide();
    //     $('#datedelivery').validationEngine('hide');
    //     var selectedDate = new Date(ev.date);
    //     var today = new Date('<?= $now ?>');
    //     if (today.valueOf() == selectedDate.valueOf()) {
    //         $('.messageShow').html('<?= $message ?>').parent().addClass('has-warning');
    //         $('.btn-next').hide();
    //     } else {
    //         $('.messageShow').html('').parent().removeClass('has-warning');
    //         $('.btn-next').show();
    //     }

    // });

    });

    $('.btn-next').show();
</script>
