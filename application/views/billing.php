<script>
$(function(){
    <?php
    if (@$prev_form) {
        ?>
		$.mc.prev_form = '<?= $prev_form ?>';
		<?php

}
?>
    $.mc.next_form = '<?= $next_form ?>';
});
</script>

<div id="merchandise_wrapper">
    <?php $this->view('static-views/breadcrumb', array('shops' => $shops)); ?>

    <!-- <div class=''>
       <div class='col-lg-12'>
            <p>Substitutions of an arrangement and container of similar value may be made in the unlikely event that your selection is unavailable. </p>

            <p>Please allow 24 hours for delivery. Orders placed after 4pm on Fri. will not be available for delivery until the following Monday after 11 am.</p>
        </div>
    </div> -->

    <div class='container-billing'>
        <form role="form" action="" method="post" id='mainform'>
        <input type="hidden" name="scriptaction" 	id="scriptaction"	value="validate" />
        <input type="hidden" name="next_form" 		id="next_form"		value="<?= $next_form ?>" />
        <div class='row-fluid'>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                    <legend>Billing Information</legend>
                    <div class='form-group'>
                        <label for='firstname'>First Name</label>
                        <input type='text' name='firstname' id='firstname' class='form-control validate[required]' placeholder='First Name'/>
                    </div>

                    <div class='form-group'>
                        <label for='lastname'>Last Name</label>
                        <input type='text' name='lastname' id='lastname' class='form-control validate[required]' placeholder='Last Name'/>
                    </div>

                    <div class='form-group'>
                        <label for='email'>E-mail</label>
                        <input type='text' class='form-control validate[required,custom[email]]' name='email' id='email' placeholder='E-mail' data-value='kapom@frazerconsultants.com' />
                    </div>

                    <div class='form-group'>
                        <label for='streetaddress'>Street Address</label>
                        <input type='text' class='form-control validate[required]' name='streetaddress' id='streetaddress' placeholder='Street Address'/>
                    </div>

                    <div class='form-group'>
                        <label for='city'>City</label>
                        <input type='text' class='form-control validate[required]' name='city' id='city' placeholder='City'/>
                    </div>

                    <div class='form-group'>
                        <label for='state'>State</label>
                        <select class='form-control validate[required]' name='state'>
                            <?php foreach ($cfg['states'] as $key => $group) : ?>
                            <option value='<?php echo $group['code'] ?>'><?php echo $group['name'] ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>

                    <div class='form-group'>
                        <label for='zip'>Zip</label>
                        <input type='text' class='form-control validate[required,custom[onlyLetterNumber]]' name='zip' id='zip' placeholder='Zip'/>
                    </div>

                    <div class='form-group'>
                        <label for='daytimephone'>Daytime Phone</label>
                        <input type='text' class='form-control validate[required]' name='daytimephone' id='daytimephone' placeholder='Daytime Phone'/>
                    </div>

            </div>

            <?php $this->load->view('summary_cart'); ?>

        </div>
        </form>
    <div class="clearfix"></div>
    <div class='contain-step'>
        <div class='pull-right'>
            <a class='merchant-btn btn-back' href="#">Return to Cart</a>
            <a class='merchant-btn-revert btn-next' href="#">Continue</a>
        </div>
    </div>
    </div>
</div>
<script src="<?= ASSETPATH ?>/js/billing.js"></script>
<script>
$(function(){
    <?php if (!empty($user_session['billing'])) { ?>
    $("#mainform").jsonToElement({
        json: <?= json_encode($user_session['billing']) ?>
    });
    <?php 
} ?>
});
</script>
