<link rel="stylesheet" href="<?=ASSETPATH?>/css/products.css<?=is_dev() ? "?_".date("YmdHis") : ""?>" />
<style>

    #merchandise_wrapper #breadcrumb #cart span.left {
        background-image: url(<?php echo DEFAULT_FRONT_IMAGE; ?>qty-left.jpg);
    }
    #merchandise_wrapper #breadcrumb #cart span.right {
        background-image: url(<?php echo DEFAULT_FRONT_IMAGE; ?>qty-right.jpg);
    }

</style>
<style>


</style>
<div id="alertmsg1" class="alert alert-success flyover flyover-top"><h4>Success</h4>
    <p> You have added product to your shopping cart!</p></div>
<div id="merchandise_wrapper">

    <?php $this->view('static-views/breadcrumb', array('shops' => $shops)); ?>

    <!-- <p>Substitutions of an arrangement and container of similar value may be made in the unlikely event that your selection is unavailable.<br><br>Please allow 24 hours for delivery. Orders placed after 4pm on Fri. will not be available for delivery until the following Monday after 11 am.</p> -->

    <h3 style="margin-bottom:25px; text-transform:capitalize;">Products</h3>
    <?php $this->view('static-views/products_list'); ?>

</div>
<script>
    var shopname = "<?php echo $shops['name'] ?>";
</script>
<script src="<?=ASSETPATH?>/js/product.js<?=TAILSTRING?>"></script>